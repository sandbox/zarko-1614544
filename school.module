<?php

/**
 * Implements hook_node_info().
 */
function school_node_info() {
  $items = array(
    'course' => array(
      'name' => t('Course'),
      'base' => 'node_content',
      'description' => t('This is an individual course.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'series' => array(
      'name' => t('Series'),
      'base' => 'node_content',
      'description' => t('This is a series of three related courses.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implements hook_commerce_product_type_info().
 */
function school_commerce_product_type_info() {
  $product_types = array();

  $product_types['quarter_class'] = array(
    'type' => 'quarter_class',
    'name' => t('Quarter Class'),
    'description' => t('A class which occurs during a normal quarter.  It is on one day of the week at a specific time for the entire quarter.'),
  );
  $product_types['series'] = array(
    'type' => 'series',
    'name' => t('Series'),
    'description' => t('A series of 3 quarter classes.'),
  );
  $product_types['summer_camp'] = array(
    'type' => 'summer_camp',
    'name' => t('Summer Camp'),
    'description' => t('A class which occurs during the summer.  It is a week long class offered each day of the week at a specific time.'),
  );
  $product_types['day_class'] = array(
    'type' => 'day_class',
    'name' => t('Day Class'),
    'description' => t('A DIY type class that takes place on a single day.'),
  );

  return $product_types;
}

/**
 * Implements hook_commerce_line_item_type_info().
 */
function school_commerce_line_item_type_info() {
  $line_item_types = array();

  $line_item_types['class'] = array(
    'type' => 'class',
    'name' => t('Class'),
    'description' => t('References a class and displays it with class specific information as the label.'),
    'product' => TRUE,
    'add_form_submit_value' => t('Add class'),
    'base' => 'commerce_product_line_item',
    'callbacks' => array(
      'configuration' => 'school_class_configuration',
    ),
  );

  return $line_item_types;
}

function school_class_configuration($line_item_type) {
  $type = $line_item_type['type'];

  commerce_product_line_item_configuration($line_item_type);

  _school_create_field(array(
    'cardinality' => 1,
    'entity_types' => array('commerce_line_item'),
    'field_name' => 'student',
    'settings' => array(
      'max_length' => '255',
    ),
    'translatable' => FALSE,
    'type' => 'text',
  ));
  _school_create_field_instance(array(
    'bundle' => $type,
//TODO why did I have to go check the "show on add to cart form"?
    'commerce_cart_settings' => array(
      'field_access' => TRUE,
    ),
    'display' => array(
      'default' => array(
         'label' => 'inline',
         'type' => 'text_default',
         'weight' => '0',
      ),
    ),
    'entity_type' => 'commerce_line_item',
    'field_name' => 'student',
    'label' => t('Student'),
    'required' => TRUE,
    'widget' => array(
      'active' => 1,
      'type' => 'text_textfield',
      'weight' => '0',
      'settings' => array(
        'size' => '20',
      ),
    ),
  ));
}

/**
 * hook_commerce_customer_profile_type_info
 * Defines customer profile types used to collect customer information during
 * checkout and order administration.
 */
function school_commerce_customer_profile_type_info() {
  $profile_types = array();

//  $profile_types['parent'] = array(
//    'type' => 'parent',
//    'name' => t('Contact information'),
//    'description' => t('The profile used to collect contact information on the checkout and order forms.'),
//  );
//
//  $profile_types['student'] = array(
//    'type' => 'student',
//    'name' => t('Student information'),
//    'description' => t('The profile used to collect student information on the checkout and order forms.'),
//  );

  $profile_types['paymentoptions'] = array(
    'type' => 'paymentoptions',
    'name' => t('Payment information'),
    'description' => t('The profile used to collect payment and charter school information on the checkout and order forms.'),
  );

  return $profile_types;
}

/* *
 * hook_commerce_customer_profile_type_info_alter
 * Allows modules to alter customer profile types defined by other modules.
 * /
function school_commerce_customer_profile_type_info_alter(&$profile_types) {
}

/**
 * hook_commerce_customer_profile_uri
 * Allows modules to specify a uri for a customer profile.
 * /
function school_commerce_customer_profile_uri($order) {
}

/**
 * hook_commerce_customer_profile_presave
 * Allows you to prepare customer profile data before it is saved.
 * /
function school_commerce_customer_profile_presave($profile) {
}

/**
 * hook_commerce_customer_profile_can_delete
 * Determines whether or not a given customer profile can be deleted.
 * /
function school_commerce_customer_profile_can_delete($profile) {
}
*/

/**
 * Implements hook_views_api().
 */
function school_views_api($module, $api) {
  if ($module == 'views' && $api == 'views_default') {
    return array('version' => 2);
  }
}

/*
 * Create or update taxonomy.
 */
function _school_create_taxonomy($vocabulary, $reset = FALSE) {
  $existing_vocab = taxonomy_vocabulary_machine_name_load($vocabulary['machine_name']);
  $vocabulary = (object) $vocabulary;
  if ($existing_vocab) {
    if ($reset) {
      $vocabulary->vid = $existing_vocab->vid;
      taxonomy_vocabulary_save($vocabulary);
    }
  }
  else {
    taxonomy_vocabulary_save($vocabulary);
  }
}

/*
 * Create or update field.
 */
function _school_create_field($field_config, $reset = FALSE) {
  $existing_field = field_info_field($field_config['field_name']);
  if (!empty($existing_field)) {
    if ($reset && ($field_config + $existing_field != $existing_field)) {
      field_update_field($field_config);
    }
  }
  else {
    field_create_field($field_config);
  }
}

/*
 * Create or update field instance.
 */
function _school_create_field_instance($field_instance, $reset = FALSE) {
  $existing_instance = field_info_instance($field_instance['entity_type'], $field_instance['field_name'], $field_instance['bundle']);
  if (!empty($existing_instance)) {
    if ($reset && ($field_instance + $existing_instance != $existing_instance)) {
      field_update_instance($field_instance);
    }
  }
  else {
    field_create_instance($field_instance);
  }
}
